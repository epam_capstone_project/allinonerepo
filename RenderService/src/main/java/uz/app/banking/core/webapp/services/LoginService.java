package uz.app.banking.core.webapp.services;

import org.springframework.stereotype.Service;

import uz.app.banking.core.webapp.references.UserType;

@Service
public class LoginService {
    public String getToken(String login, String password) throws Exception {
        return login;
    }

    public UserType getUserType(String token) throws Exception {
        if(token.contains("emp")) return UserType.EMPLOYEE;
        else if(token.contains("admin")) return UserType.ADMIN;
        else if(token.contains("client")) return UserType.CLIENT;
        else throw new RuntimeException("Invalid token");
    }
}
