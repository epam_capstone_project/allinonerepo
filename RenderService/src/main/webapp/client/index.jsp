<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="../css/common.css">

	<script type="text/javascript" src="../js/jquery-3.7.1.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	<link rel="stylesheet" type="text/css" href="../fa/css/all.css" />
	<title>Golden Coin Community Bank - Banking System</title>
</head>
<body>
	<div id="sidebar">
		<div class="list-group w-100 h-100 border-right bg-light">
			<a href="index.html" class="list-group-item list-group-item-action d-flex rounded-0">
				<div>
			    <span style="font-weight: bold; font-style:italic; font-size: 24pt;">
			    	<i class="fa-solid fa-coins"></i>
			    </span>
			    Golden Coin Community Bank
		  	</div>
			</a>
		  <a href="index.html" class="list-group-item list-group-item-action d-flex rounded-0 active">
		  	<i class="fa-solid fa-house"></i> Main
		  </a>
		  <a href="accounts.html" class="list-group-item list-group-item-action d-flex rounded-0">
		    <i class="fa-solid fa-wallet"></i> My accounts
		    <span class="badge badge-primary badge-pill">5</span>
		  </a>
		  <a href="transactions.html" class="list-group-item list-group-item-action d-flex rounded-0">
		    <i class="fa-solid fa-file-invoice-dollar"></i> Transactions
		    <span class="badge badge-primary badge-pill">1348</span>
		  </a>
		  <a href="analytics.html" class="list-group-item list-group-item-action d-flex rounded-0">
		    <i class="fa-solid fa-chart-simple"></i> Analytics
		  </a>
		  <a href="profile.html" class="list-group-item list-group-item-action d-flex rounded-0">
		    <i class="fa-solid fa-user"></i> Profile
		  </a>
		</div>
	</div>

	<div id="header" class="border-bottom bg-white">
		<div class="row py-2">
	    <div class="col-4 text-left">
    		<span class="badge badge-secondary">Operational date:</span> <span class="badge badge-primary">04.03.2024</span>
	    </div>
	    <div class="col-6 text-right">
	    	<span class="badge badge-secondary">User:</span><span class="badge badge-primary">Mark Butowski</span><br>
	    </div>
	    <div class="col-2 text-right">
	      <a href="../login.html">
	      	<button class="btn btn-secondary"><i class="fa-solid fa-right-from-bracket"></i> Logout</button>
	      </a>
	    </div>
	  </div>
	</div>
	
	<div id="content">
		<jsp:include page="${page}.jsp" />
	</div>

</body>
</html>