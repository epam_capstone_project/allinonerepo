function getFormData(form) {
  var result = {};

  var inputs = form.getElementsByTagName("input");
  var selects = form.getElementsByTagName("select");
  var textareas = form.getElementsByTagName("textarea");

  for(var i = 0; i < inputs.length; i++){
    if(typeof result[inputs[i].name] === 'undefined')
      result[inputs[i].name] = inputs[i].value;
    else if (result[inputs[i].name] instanceof Array)
      result[inputs[i].name].push(inputs[i].value);
    else{
      result[inputs[i].name] = [result[inputs[i].name]];
      result[inputs[i].name].push(inputs[i].value);
    }
  }

  for(var i = 0; i < selects.length; i++)
    result[selects[i].name] = selects[i].value;

  for(var i = 0; i < textareas.length; i++)
    result[textareas[i].name] = textareas[i].value;

  return result;
}

function isDef(obj) {
  return (typeof obj !== 'undefined');
}