package uz.app.banking.domain;

import uz.app.banking.entities.User;

public class UserDTO extends UserInfoDTO {
    private String password;
    private String passwordConfirm;
    
    public UserDTO() {};

    public UserDTO(User user) {
        super(user);
    }

    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public String getPasswordConfirm() {
        return passwordConfirm;
    }
    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }
}
