package uz.app.banking.domain;

public class SavingResponseDTO<T> {
    private T data;

    private String errCode;
    private String msg;

    public SavingResponseDTO(T data, String errCode, String msg) {
        this.data = data;
        this.errCode = errCode;
        this.msg = msg;
    }

    public T getData() {
        return data;
    }
    public String getErrCode() {
        return errCode;
    }
    public String getMsg() {
        return msg;
    }
}
