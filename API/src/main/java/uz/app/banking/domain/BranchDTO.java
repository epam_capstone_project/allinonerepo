package uz.app.banking.domain;

import uz.app.banking.entities.Branch;

public class BranchDTO {
    private String code;
    private String name;
    private String region;

    public BranchDTO(Branch branch) {
        this.code = branch.getCode();
        this.name = branch.getName();
        this.region = branch.getRegion();
    }
    
    public BranchDTO(String code, String name, String region) {
        this.code = code;
        this.name = name;
        this.region = region;
    }
    public String getCode() {
        return code;
    }
    public String getName() {
        return name;
    }
    public String getRegion() {
        return region;
    }

    
}
