package uz.app.banking.domain;

import java.time.LocalDateTime;
import java.util.List;

import uz.app.banking.entities.User;
import uz.app.banking.references.UserType;

public class UserInfoDTO {
    private Long id;
    private String login;
    private UserType userType;
    private LocalDateTime createdAt;
    private UserInfoDTO createdBy;
    
    public UserInfoDTO() {};

    public UserInfoDTO(User user) {
        this.id = user.getUserId();
        this.login = user.getLogin();
        this.userType = user.getUserType();
        this.createdAt = user.getCreatedAt();
        this.createdBy =  (user.getCreatedBy() != null)?new UserInfoDTO(user.getCreatedBy()):null;
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getLogin() {
        return login;
    }
    public void setLogin(String login) {
        this.login = login;
    }
    public UserType getUserType() {
        return userType;
    }
    public void setUserType(UserType userType) {
        this.userType = userType;
    }
    public LocalDateTime getCreatedAt() {
        return createdAt;
    }
    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }
    public UserInfoDTO getCreatedBy() {
        return createdBy;
    }
    public void setCreatedBy(UserInfoDTO createdBy) {
        this.createdBy = createdBy;
    }
}
