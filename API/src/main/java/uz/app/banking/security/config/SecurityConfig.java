package uz.app.banking.security.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import uz.app.banking.security.filters.AuthenticationFilter;
import uz.app.banking.security.util.JwtUtil;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(
    securedEnabled = true,
    prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private JwtUtil jwtUtil;

    @Override
    @Bean(BeanIds.AUTHENTICATION_MANAGER)
    // overloaded only for this and @bean so that we can auto-wire in our controllers
    protected AuthenticationManager authenticationManager() throws Exception {
        return super.authenticationManager();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    @Override
    public UserDetailsService userDetailsService() {
        /*UserDetails client = User.builder()
            .username("client")
            .password(passwordEncoder().encode("asd123+"))
            .roles("CLIENT")
            .build();

        UserDetails employee = User.builder()
            .username("employee")
            .password(passwordEncoder().encode("asd123+"))
            .roles("EMPLOYEE")
            .build();

        UserDetails admin = User.builder()
            .username("admin")
            .password(passwordEncoder().encode("asd123+"))
            .roles("ADMIN")
            .authorities("ROLE_ADMIN")
            .build();

        return new InMemoryUserDetailsManager(client, employee, admin);*/

        return new uz.app.banking.security.services.UserDetailsService();
    }

    @Bean
    public AuthenticationFilter authenticationFilter() throws Exception {
        return new AuthenticationFilter(userDetailsService(), authenticationManager(), jwtUtil);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors() //Cross-Origin Resource Sharing
                .and()
                .csrf().disable()   //Cross-Site Request Forgery https://www.baeldung.com/spring-security-csrf
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS) //REST Apis are stateless
                .and()
                .authorizeRequests()
                .antMatchers("/login", "/register").permitAll()
                .anyRequest().authenticated() // rest any other request should be authenticated
                .and()
                .addFilterBefore(authenticationFilter(), UsernamePasswordAuthenticationFilter.class);
    }
}
