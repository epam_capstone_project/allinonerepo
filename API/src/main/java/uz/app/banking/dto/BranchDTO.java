package uz.app.banking.dto;

import uz.app.banking.entities.Branch;

public class BranchDTO {
    private String code;
    private String name;
    private String region;

    public BranchDTO() {    }

    public static BranchDTO toDTO(Branch branch) {
        if(branch == null) return null;
        BranchDTO dto = new BranchDTO();
        dto.code = branch.getCode();
        dto.name = branch.getName();
        dto.region = branch.getRegion();
        return dto;
    }

    public static Branch fromDTO(BranchDTO branchDTO) {
        if(branchDTO == null) return null;
        Branch branch = new Branch(branchDTO.code, branchDTO.name, branchDTO.region);
        return branch;
    }
    
    public BranchDTO(String code, String name, String region) {
        this.code = code;
        this.name = name;
        this.region = region;
    }
    public String getCode() {
        return code;
    }
    public String getName() {
        return name;
    }
    public String getRegion() {
        return region;
    }

    
}
