package uz.app.banking.dto;

public class SavingResponseDTO<T> extends StandardResponseDTO {
    private T data;

    public SavingResponseDTO(T data, String errCode, String msg) {
        super(errCode, msg);
        this.data = data;
    }

    public T getData() {
        return data;
    }
}
