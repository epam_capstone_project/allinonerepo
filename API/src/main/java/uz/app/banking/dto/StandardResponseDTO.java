package uz.app.banking.dto;

public class StandardResponseDTO {
    
    private String errCode;
    private String msg;

    public StandardResponseDTO(String errCode, String msg) {
        this.errCode = errCode;
        this.msg = msg;
    }

    public String getErrCode() {
        return errCode;
    }
    public String getMsg() {
        return msg;
    }
}
