package uz.app.banking.dto;

public class AccountCreateRespDTO {
    private Long accId;
    private String fullAcc;
    public Long getAccId() {
        return accId;
    }
    public void setAccId(Long accId) {
        this.accId = accId;
    }
    public String getFullAcc() {
        return fullAcc;
    }
    public void setFullAcc(String fullAcc) {
        this.fullAcc = fullAcc;
    }
    public AccountCreateRespDTO() {
    }

    
}
