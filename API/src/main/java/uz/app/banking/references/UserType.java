package uz.app.banking.references;

import java.util.Arrays;

public enum UserType {
    ADMIN(0, "Admin user"),
    EMPLOYEE(1, "Employee user"),
    CLIENT(2, "Client user");

    private int code;
    private String name;
    private UserType(int code, String name) {
        this.code = code;
        this.name = name;
    }

    public int getCode() {
        return this.code;
    }

    public String getName() {
        return this.name;
    }

    public static UserType getByCode(int code) {
        return Arrays.asList(UserType.values()).stream()
            .filter(x -> x.code == code)
            .findFirst().orElseThrow();
    }
}
