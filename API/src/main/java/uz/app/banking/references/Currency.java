package uz.app.banking.references;

import java.util.stream.Stream;

public enum Currency {
    EURO("978", "Euro"),
    USD("840", "US dollars"),
    RUB("643", "Russian rouble"),
    GBP("826", "GB Pound sterling"),
    KZT("398", "Kazakh tenge"),
    UZS("860", "Uzbek sum")
    ;

    private String code;
    public String getCode() {
        return code;
    }

    private String name;
    public String getName() {
        return name;
    }

    private Currency(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static Currency getByCode(String code) {
        return Stream.of(Currency.values()).filter(x -> x.getCode().equals(code)).findFirst().get();
    }
}
