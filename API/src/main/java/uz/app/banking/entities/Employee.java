package uz.app.banking.entities;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity(name = "Employees")
@Table(schema = "public", name = "Employees")
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "emp_id")
    private Long empId;
    @Column(name = "first_name")
    private String firstName;
    @Column(name = "last_name")
    private String lastName;
    @Column(name = "middle_name")
    private String middleName;
    @JoinColumn(name = "branch_code")
    @ManyToOne
    private Branch branch;
    @Column(name = "department")
    private String department;
    @Column(name = "created_at")
    private LocalDateTime createdAt;
    @JoinColumn(name = "created_by")
    @ManyToOne
    private User createdBy;
    @Column(name = "user_id")
    private Long userId;

    /*@JoinColumn(name = "user_id")
    @OneToOne(fetch = FetchType.LAZY)
    private User user;*/

    
    public Long getEmpId() {
        return empId;
    }
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public String getMiddleName() {
        return middleName;
    }
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }
    public Branch getBranch() {
        return branch;
    }
    public String getDepartment() {
        return department;
    }
    public void setDepartment(String department) {
        this.department = department;
    }
    public LocalDateTime getCreatedAt() {
        return createdAt;
    }
    public User getCreatedBy() {
        return createdBy;
    }
    public Long getUserId() {
        return this.userId;
    }

    //public User getUser() { return this.user; }
}
