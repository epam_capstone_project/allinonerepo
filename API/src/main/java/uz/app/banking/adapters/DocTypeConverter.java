package uz.app.banking.adapters;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import uz.app.banking.references.DocType;

@Converter(autoApply = true)
public class DocTypeConverter implements AttributeConverter<DocType, String> {

    @Override
    public String convertToDatabaseColumn(DocType value) {
        return value.getCode();
    }

    @Override
    public DocType convertToEntityAttribute(String value) {
        return DocType.getByCode(value);
    }
    
}
