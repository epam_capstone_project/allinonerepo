package uz.app.banking.adapters;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import uz.app.banking.references.Currency;

@Converter(autoApply = true)
public class CurrencyConverter implements AttributeConverter<Currency, String>{
    @Override
    public String convertToDatabaseColumn(Currency value) {
        return value.getCode();
    }

    @Override
    public Currency convertToEntityAttribute(String value) {
        return Currency.getByCode(value);
    }
}
