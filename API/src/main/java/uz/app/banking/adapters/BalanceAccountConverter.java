package uz.app.banking.adapters;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import uz.app.banking.references.BalanceAccount;

@Converter(autoApply = true)
public class BalanceAccountConverter implements AttributeConverter<BalanceAccount, String> {

    @Override
    public String convertToDatabaseColumn(BalanceAccount value) {
        return value.getCode();
    }

    @Override
    public BalanceAccount convertToEntityAttribute(String value) {
        return BalanceAccount.getByCode(value);
    }
    
}
