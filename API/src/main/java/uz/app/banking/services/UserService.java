package uz.app.banking.services;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import uz.app.banking.dto.UserDTO;
import uz.app.banking.dto.UserInfoDTO;
import uz.app.banking.entities.User;
import uz.app.banking.repositories.UserRepository;

@Service
public class UserService {
    @Autowired
    UserRepository userRepo;

    @Autowired
    PasswordEncoder passwordEncoder;

    public UserInfoDTO getUserByLogin(String login) {
        return UserInfoDTO.toDTO(userRepo.findByLogin(login).orElseThrow());
    }
    
    public UserInfoDTO getUserById(Long Id) {
        return UserInfoDTO.toDTO(userRepo.findById(Id).orElseThrow());
    }

    public Page<UserInfoDTO> getAll(Pageable page) {
        Page<User> usersPage = userRepo.findAll(page);
        return usersPage.map(u -> UserInfoDTO.toDTO(u));
    }

    public UserInfoDTO save(UserDTO userDTO) throws Exception {
        User savingUser = null;
        if(userDTO.getPassword() == null || !userDTO.getPassword().equals(userDTO.getPasswordConfirm())) {
            throw new Exception("Password confirmation does not corresponds to actual password");
        }
        if(userDTO.getId() != null) {
            User oldUser = userRepo.findById(userDTO.getId()).orElseThrow();
            savingUser = new User(
                oldUser.getUserId(), 
                oldUser.getLogin(), 
                passwordEncoder.encode(userDTO.getPassword()), 
                oldUser.getUserType(), 
                oldUser.getCreatedAt(), 
                oldUser.getCreatedBy());
        } else {
            UserDetails currentUser = (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            savingUser = new User(
                null, 
                userDTO.getLogin(), 
                passwordEncoder.encode(userDTO.getPassword()),
                userDTO.getUserType(), 
                LocalDateTime.now(), 
                userRepo.findByLogin(currentUser.getUsername()).get()
            );
        }
        return UserInfoDTO.toDTO(userRepo.save(savingUser));
    }
}
