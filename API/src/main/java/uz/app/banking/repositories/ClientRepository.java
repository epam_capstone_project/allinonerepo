package uz.app.banking.repositories;

import java.util.Optional;

import org.springframework.data.repository.PagingAndSortingRepository;

import uz.app.banking.entities.Client;

public interface ClientRepository extends PagingAndSortingRepository<Client, Long> {
    public Optional<Client> findByUserUserId(Long userId);
}
