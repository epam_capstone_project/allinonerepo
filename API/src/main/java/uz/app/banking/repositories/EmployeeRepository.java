package uz.app.banking.repositories;

import java.util.Optional;

import org.springframework.data.repository.PagingAndSortingRepository;

import uz.app.banking.entities.Employee;

public interface EmployeeRepository extends PagingAndSortingRepository<Employee, Long> {
    public Optional<Employee> findByUserId(Long userId);
}
