INSERT INTO public.Ref_Currencies(code, name, short_name)	VALUES ('398', 'Kazakh tenge', 'KZT');
INSERT INTO public.Ref_Currencies(code, name, short_name)	VALUES ('643', 'Russian rouble', 'RUB');
INSERT INTO public.Ref_Currencies(code, name, short_name)	VALUES ('826', 'Pound sterling', 'GBP');
INSERT INTO public.Ref_Currencies(code, name, short_name)	VALUES ('840', 'US dollars', 'USD');
INSERT INTO public.Ref_Currencies(code, name, short_name)	VALUES ('860', 'Uzbek sums', 'UZS');
INSERT INTO public.Ref_Currencies(code, name, short_name)	VALUES ('978', 'Euro', 'EUR');


INSERT INTO public.Ref_Countries(code, name, short_name) VALUES ('840', 'United states of America', 'USA');
INSERT INTO public.Ref_Countries(code, name, short_name) VALUES ('860', 'Uzbekistan', 'UZB');
INSERT INTO public.Ref_Countries(code, name, short_name) VALUES ('398', 'Kazakhstan', 'KAZ');
INSERT INTO public.Ref_Countries(code, name, short_name) VALUES ('643', 'Russia', 'RUS');
INSERT INTO public.Ref_Countries(code, name, short_name) VALUES ('250', 'France', 'FRA');
INSERT INTO public.Ref_Countries(code, name, short_name) VALUES ('826', 'Great Britain', 'GBR');

INSERT INTO public.Ref_Doc_Types(code, name) VALUES(1, 'Passport');
INSERT INTO public.Ref_Doc_Types(code, name) VALUES(2, 'ID-card');
INSERT INTO public.Ref_Doc_Types(code, name) VALUES(3, 'Driver''s license');

INSERT INTO public.Ref_User_Types(code, name) VALUES(0, 'Admin user');
INSERT INTO public.Ref_User_Types(code, name) VALUES(1, 'Employee user');
INSERT INTO public.Ref_User_Types(code, name) VALUES(2, 'Client user');

INSERT INTO public.ref_balance_accounts(
	balance_acc, purpose)
VALUES
	('555', 'Client plastic card'),
	('510', 'Virtual wallet account for client'),
	('620', 'Client deposit'),
	('630', 'Client credit'),
	('200', 'Transit income'),
	('210', 'Transit outcome'),
	('001', 'Bank profit'),
	('002', 'Bank loss'),
	('101', 'Interbank IN'),
	('102', 'Interbank OUT')
;


INSERT INTO public.Branches(code, name, region) VALUES ('000', 'Tashkent Head Branch', 'Tashkent, Uzbekistan');
INSERT INTO public.Branches(code, name, region) VALUES ('100', 'Samarkand main office branch', 'Samarkand, Uzbekistan');
INSERT INTO public.Branches(code, name, region) VALUES ('200', 'Bukhara main office branch', 'Bukhara, Uzbekistan');
INSERT INTO public.Branches(code, name, region) VALUES ('300', 'Xorezm main office branch', 'Xorezm, Uzbekistan');
INSERT INTO public.Branches(code, name, region) VALUES ('400', 'Qarshi main office branch', 'Qarshi, Uzbekistan');
INSERT INTO public.Branches(code, name, region) VALUES ('500', 'Andijon main office branch', 'Andijon, Uzbekistan');

INSERT INTO public.Users(login, password_hash, user_type, created_at)	VALUES ('client', '$2a$12$2DIxYuC0fGtsS1fpX35XCuZ5ZqrCbjBr6PLQSUzLZFV6D2Ay8uVjC', 2, now());
INSERT INTO public.Users(login, password_hash, user_type, created_at)	VALUES ('employee', '$2a$12$2DIxYuC0fGtsS1fpX35XCuZ5ZqrCbjBr6PLQSUzLZFV6D2Ay8uVjC', 1, now());
INSERT INTO public.Users(login, password_hash, user_type, created_at)	VALUES ('admin', '$2a$12$2DIxYuC0fGtsS1fpX35XCuZ5ZqrCbjBr6PLQSUzLZFV6D2Ay8uVjC', 0, now());
INSERT INTO public.Users(login, password_hash, user_type, created_at)	VALUES ('some_client', '$2a$12$2DIxYuC0fGtsS1fpX35XCuZ5ZqrCbjBr6PLQSUzLZFV6D2Ay8uVjC', 2, now());


INSERT INTO public.Employees(first_name, last_name, middle_name, branch_code, department, created_at, user_id) VALUES ('Mark', 'Butowski', 'Liverman', '100', 'Department of credits', now(), 2);

INSERT INTO public.Clients(first_name, last_name, middle_name, residency, doc_type, doc_seria, doc_number, branch_code, user_id, created_at) VALUES ('Clyde', 'Griffiths', 'Dreiser', '840', '1', 'AA', '1234567', '100', 1, now());
INSERT INTO public.Clients(client_id, first_name, last_name, middle_name, residency, doc_type, doc_seria, doc_number, branch_code, user_id, created_at) VALUES (0, 'Golden', 'Coin', 'Bank', '860', '1', 'AA', '0000000', '100', 1, now());


INSERT INTO public.accounts(
	balance_acc, full_acc, currency, available_balance, actual_balance, negative_balance_enabled, client_id, branch_code, frozen, turnover_all_debit, turnover_all_credit, created_at, created_by)
	VALUES ('001', '001860100000000001', '860', 0, 0, true, 0, '100', false, 0, 0, now(), 1);